package com.novuyo.courierapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Recipient extends AppCompatActivity {

    EditText repid;
    EditText repmob;
    EditText add;

    public static String repicientid;
    public static String repicientmob;
    public static String repicientadd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipient);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "fonts/LatoRegular.ttf");
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LatoLight.ttf");

        repid = (EditText) findViewById(R.id.repid);
        repid.setTypeface(custom_font1);

        repmob = (EditText) findViewById(R.id.mob);
        repmob.setTypeface(custom_font1);



        add = (EditText) findViewById(R.id.add);
        add.setTypeface(custom_font);
        add.setVisibility(View.GONE);

        repicientadd = "N/A";

        final CheckBox ch1=(CheckBox)findViewById(R.id.checkBox1);
       ch1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    Toast.makeText(Recipient.this,
                            "Deliver to Recipient :)", Toast.LENGTH_LONG).show();
                    add.setVisibility(View.VISIBLE);

                    //repicientadd = add.getText().toString();
                }

            }
        });


        Button proceed = (Button) findViewById(R.id.btnProceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if (ch1.isChecked()) {

                    repicientadd = add.getText().toString();
                }else {
                    repicientadd = "NA";
                }



                repicientid = repid.getText().toString();
                repicientmob = repmob.getText().toString();

                System.out.println(repicientid);
                System.out.println(repicientmob);
                System.out.println(repicientadd);


                if(repicientid.equals("")){
                    Toast.makeText(Recipient.this,"Please Enter Recipient ID",Toast.LENGTH_LONG).show();
                }else if(repicientmob.equals("")){

                    Toast.makeText(Recipient.this,"Please Enter Recipient Mobile",Toast.LENGTH_LONG).show();
                }else{
                    Intent myIntent = new Intent(Recipient.this,
                            RequestInfo.class);
                    startActivity(myIntent);

                }



            }
        });
    }




}
