package com.novuyo.courierapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class RequestInfo extends AppCompatActivity {

    EditText add;
    EditText collect;
    EditText desc;

    public static String collectadd;
    public static String description;
    public static String excess;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "fonts/LatoRegular.ttf");
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LatoLight.ttf");

        collect = (EditText) findViewById(R.id.collect);
        collect.setTypeface(custom_font1);

        desc = (EditText) findViewById(R.id.desc);
        desc.setTypeface(custom_font1);



        add = (EditText) findViewById(R.id.add);
        add.setTypeface(custom_font);
        add.setVisibility(View.GONE);


        excess = "0";

        final CheckBox ch1=(CheckBox)findViewById(R.id.checkBox1);
        ch1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    Toast.makeText(RequestInfo.this,
                            "Excess KG :)", Toast.LENGTH_LONG).show();
                    add.setVisibility(View.VISIBLE);

                    //excess = add.getText().toString();

                }

            }
        });

        Button proceed = (Button) findViewById(R.id.btnProceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if (ch1.isChecked()) {

                excess = add.getText().toString();

                }else{

                    excess = "NA";

                }

                collectadd = collect.getText().toString();
                description = desc.getText().toString();


                if(collectadd.equals("")){
                    Toast.makeText(RequestInfo.this,"Please Enter Your Address",Toast.LENGTH_LONG).show();
                }else if(description.equals("")){

                    Toast.makeText(RequestInfo.this,"Please Enter Description",Toast.LENGTH_LONG).show();
                }else{
                    Intent myIntent = new Intent(RequestInfo.this,
                            Payment.class);
                    startActivity(myIntent);
                }
            }
        });
    }

}
