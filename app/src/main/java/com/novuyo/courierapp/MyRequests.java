package com.novuyo.courierapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.android.volley.toolbox.JsonArrayRequest;


import org.json.JSONArray;

import org.json.JSONObject;


import android.util.Log;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import java.util.ArrayList;


public class MyRequests extends AppCompatActivity {

    private String urlJsonArry = "http://192.168.1.197:9091/requests";
    private String jsonResponse;

    private static String TAG = MyRequests.class.getSimpleName();


    // Progress dialog
    private ProgressDialog pDialog;

    private ListView lv;
    ArrayList<String> myrequests = new ArrayList<String>();

    DataBaseAdapter dataBaseAdapter;
    final String KEY_ID = "requestID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dataBaseAdapter=new DataBaseAdapter (this);
        dataBaseAdapter=dataBaseAdapter.open();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        lv = (ListView) findViewById(R.id.list);


        makeJsonArrayRequest();


    }

    private void makeJsonArrayRequest() {

        showpDialog();

        JsonArrayRequest req = new JsonArrayRequest(urlJsonArry,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        try {

                            // System.out.println(response.toString());
                            //Parsing json array response
                            //loop through each json object

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject request = (JSONObject) response
                                        .get(i);

                                String id = request.getString("id");
                                String username = request.getString("username");
                                String password = request.getString("password");
                                String recipientid = request.getString("recipientid");
                                String repmob = request.getString("repmob");
                                String collectadd = request.getString("collectadd");
                                String excess = request.getString("excess");
                                String paytype = request.getString("paytype");
                                String total = request.getString("total");
                                String description = request.getString("description");
                                String deliveryaddress = request.getString("description");




                                System.out.println(id + "\n\n");

                                myrequests.add(id);

                                dataBaseAdapter.insertEntry(id,username,password,recipientid,repmob,collectadd,excess,paytype,total,description,deliveryaddress);


                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }

                        hidepDialog();
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyRequests.this,
                                android.R.layout.simple_list_item_1, android.R.id.text1, myrequests) {
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                // Get the current item from ListView
                                View view = super.getView(position, convertView, parent);
                                if (position % 2 == 1) {
                                    // Set a background color for ListView regular row/item
                                    view.setBackgroundColor(Color.parseColor("#fefce8"));
                                } else {
                                    // Set the background color for alternate row/item
                                    view.setBackgroundColor(Color.parseColor("#E67E22"));
                                }
                                return view;
                            }
                        };


                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {


                                String request_id = ((TextView) view.findViewById(android.R.id.text1)).getText().toString();
                                System.out.println("OD"+ request_id);

                                // Starting new intent
                                Intent in = new Intent(getApplicationContext(), RequestDetail.class);
                                in.putExtra(KEY_ID, request_id);

                                startActivity(in);

                            }
                        });

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println(error.getMessage());

                hidepDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);


    }



    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
