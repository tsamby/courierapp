package com.novuyo.courierapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class RequestDetail extends AppCompatActivity {

    DataBaseAdapter dataBaseAdapter;
    final String KEY_ID = "requestID";
    ListView lv;
    public static ArrayList<HashMap<String, String>> request_detail = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dataBaseAdapter = new DataBaseAdapter(this);
        dataBaseAdapter = dataBaseAdapter.open();

        // getting intent data
        Intent in = getIntent();

        // Get XML values from previous intent
        String request_id = in.getStringExtra(KEY_ID);

        // Displaying all values on the screen
        TextView lblOrderID = (TextView) findViewById(R.id.txtorderid);
        lblOrderID.setText("REQUEST ID :" + " " + request_id);


        request_detail.clear();

      /*  try {
            Cursor c = dataBaseAdapter.db.rawQuery("SELECT * FROM REQUESTS WHERE REQUESTID='" + request_id + "'", null);


            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String repid = c.getString(c.getColumnIndex("RECIPIENTID"));
                        System.out.println(repid);
                        //String repmob = c.getString(c.getColumnIndex("REPMOB"));
                        //System.out.println(repmob);


                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put("repid", repid);
                        ///map.put("repmob", repmob);
                        map.put("status", "In Transit");
                        request_detail.add(map);

                    } while (c.moveToNext());


                 }else {

                }


            } else {


                return;
            }
            */

        try {


            try {
                Cursor c = dataBaseAdapter.db.rawQuery("SELECT * FROM REQUESTS WHERE REQUESTID='" + request_id + "'", null);

                if (c.moveToFirst()) {

                    String repid = c.getString(c.getColumnIndex("RECIPIENTID"));
                    System.out.println(repid);
                    //String repmob = c.getString(c.getColumnIndex("REPMOB"));
                    //System.out.println(repmob);


                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("repid", repid);
                    ///map.put("repmob", repmob);
                    map.put("status", "In Transit");
                    request_detail.add(map);


                } else {

                }
            } catch (Exception ee) {
                System.out.println(ee.getMessage());


    }

            lv = (ListView) findViewById(R.id.list);
            android.widget.ListAdapter listAdapter = new SimpleAdapter(RequestDetail.this, request_detail,
                    R.layout.request_detail,
                    new String[]{"repid",  "status"}, new int[]{
                    R.id.repID,  R.id.txtStatus});

            lv.setAdapter(listAdapter);


        }catch (Exception ex){

            System.out.println(ex.getStackTrace());
        }
    }
}
