package com.novuyo.courierapp;

/**
 * Created by jason on 2018/09/06.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DataBaseAdapter {



    static final String DATABASE_NAME = "orderingapp.db";
    static final int DATABASE_VERSION = 1;
    public static final int NAME_COLUMN = 1;

    // TODO: Create public field for each column in your table.
    // SQL Statement to create a new database.

    static final String CREATE_DATABASE = "create table " + "REQUESTS" +
            "( " + "ID" + " integer primary key autoincrement," + "REQUESTID" + "  text," + "USERNAME" + "  text," + "PASSWORD" + "  text," + "RECIPIENTID" + "  text," + "REPMOB" + "  text," + "COLLECTADD" + "  text," + "EXCESS" + "  text," + "PAYTYPE" + "  text," + "TOTAL" + "  text," + "DESCRIPTION" + "  text," + "DELIVERYADDRESS" + "  text); ";


    // Variable to hold the database instance
    public SQLiteDatabase db;
    // Context of the application using the database.
    private final Context context;
    // Database open/upgrade helper
    private DataBaseHelper dbHelper;

    public DataBaseAdapter(Context _context) {
        context = _context;
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DataBaseAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        db.close();
    }

    public SQLiteDatabase getDatabaseInstance() {
        return db;
    }

    public void insertEntry(String id,String username,String password,String recipientid,String repmob,String collectadd,String excess,String paytype,String total,String description,String deliveryaddress) {
        ContentValues newValues = new ContentValues();
        // Assign values for each row.
        newValues.put("REQUESTID", id);
        newValues.put("USERNAME", username);
        newValues.put("PASSWORD", password);
        newValues.put("RECIPIENTID", recipientid);
        newValues.put("REPMOB", repmob);
        newValues.put("COLLECTADD", collectadd);
        newValues.put("EXCESS", excess);
        newValues.put("PAYTYPE", paytype);
        newValues.put("TOTAL", total);
        newValues.put("DESCRIPTION", description);
        newValues.put("DELIVERYADDRESS", deliveryaddress);

        // Insert the row into your table
        db.insert("REQUESTS", null, newValues);

    }

}




