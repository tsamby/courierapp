package com.novuyo.courierapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TableRow.LayoutParams;

public class Payment extends AppCompatActivity {

    TableLayout tl;
    TableRow tr;
    TextView companyTV,valueTV;
    TextView txtTotal;

    String _shippingAddress [] = {"FNB","6236525896","256025"};
    String _column1 [] ={"Bank","Account Number","Branch Code"};

    public  static  String pay_tye;
    public  static  String total;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        tl = (TableLayout) findViewById(R.id.maintable);
        addHeaders();
        addData();

        txtTotal = (TextView) findViewById(R.id.txtOrderTotal);
        total = txtTotal.getText().toString();

        Button cash = (Button) findViewById(R.id.btnCash);
        cash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                pay_tye = "Cash";

                Intent myintent = new Intent(Payment.this,SubmitRequest.class);
                startActivity(myintent);

            }
        });


        Button deposit = (Button) findViewById(R.id.btnDeposit);
        deposit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                pay_tye = "Deposit";

                Intent myintent = new Intent(Payment.this,SubmitRequest.class);
                startActivity(myintent);

            }
        });



        Button card = (Button) findViewById(R.id.btnCard);
        card.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                pay_tye = "Card";

                Intent myintent = new Intent(Payment.this,SubmitRequest.class);
                startActivity(myintent);

            }
        });





    }


    /** This function add the headers to the table **/
    public void addHeaders(){

        /** Create a TableRow dynamically **/
        tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating a TextView to add to the row **/
        TextView companyTV = new TextView(this);
        //companyTV.setText("Shipping Address");
        //companyTV.setTextColor(Color.GRAY);
        //companyTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        //companyTV.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
        //companyTV.setPadding(5, 5, 5, 0);
       // tr.addView(companyTV);  // Adding textView to tablerow.

        /** Creating another textview **/
        TextView valueTV = new TextView(this);
        valueTV.setText("");
        valueTV.setTextColor(Color.GRAY);
        valueTV.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
        valueTV.setPadding(5, 5, 5, 0);
        valueTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(valueTV); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        // we are adding two textviews for the divider because we have two columns
        tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating another textview **/
        TextView divider = new TextView(this);

        divider.setTextColor(Color.GREEN);
        divider.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
        divider.setPadding(5, 0, 0, 0);
        divider.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider); // Adding textView to tablerow.

        TextView divider2 = new TextView(this);
        divider2.setTextColor(Color.GREEN);
        divider2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
        divider2.setPadding(5, 0, 0, 0);
        divider2.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider2); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
    }

    /** This function add the data to the table **/
    public void addData() {

        for (int i = 0; i < _column1.length; i++) {
            /** Create a TableRow dynamically **/
            tr = new TableRow(this);
            tr.setLayoutParams(new LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));

            /** Creating a TextView to add to the row **/
            companyTV = new TextView(this);
            companyTV.setText(_column1[i]);
            companyTV.setTextColor(Color.BLACK);
            companyTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            companyTV.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            companyTV.setPadding(5, 5, 5, 5);
            tr.addView(companyTV);  // Adding textView to tablerow.

            /** Creating another textview **/
            valueTV = new TextView(this);
            valueTV.setText(_shippingAddress[i]);
            valueTV.setTextColor(Color.BLUE);
            valueTV.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            valueTV.setPadding(5, 5, 5, 5);
            valueTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            tr.addView(valueTV); // Adding textView to tablerow.

            // Add the TableRow to the TableLayout
            tl.addView(tr, new TableLayout.LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));
        }
    }

}
