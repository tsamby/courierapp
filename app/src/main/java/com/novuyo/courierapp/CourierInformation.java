package com.novuyo.courierapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CourierInformation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_information);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "fonts/LatoRegular.ttf");
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/LatoLight.ttf");


        TextView info = (TextView) findViewById(R.id.login);
        info.setTypeface(custom_font1);

        info.setTypeface(custom_font);


        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(CourierInformation.this,WareHouseInfo.class);
                startActivity(it);

            }
        });

        Button proceed = (Button) findViewById(R.id.btnProceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent myIntent = new Intent(CourierInformation.this,
                        Recipient.class);
                startActivity(myIntent);

            }
        });

    }

}
